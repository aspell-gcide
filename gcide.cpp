/* Aspell filter for GCIDE dictionary corpus
   Copyright (C) 2024 Sergey Poznyakoff

   Aspell-gcide is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Aspell-gcide is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with aspell-gcide. If not, see <http://www.gnu.org/licenses/>. */

#include <string>
#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>

#include <config.hpp>
#include <indiv_filter.hpp>
#include "string_map.hpp"

#define C 0x0100       // Close
#define O 0x0200       // Open
#define S 0x0400       // Store
#define R 0x0800       // Reset storage
#define V 0x1000       // Visible
#define STATE_OF(c) (c & 0xff)
#define ALPHABET " !\"-/<>\\"

namespace {
	using namespace acommon;

	class GCIDEFilter : public IndividualFilter {
	private:
		static const char alpha[];
		static const int dfl = sizeof(ALPHABET)-1;
		StringMap include_tags;
		StringMap exclude_tags;
		std::string *in_tag;
		std::string *ex_tag;
		std::string storage;

		inline int c2a(FilterChar::Chr c)
		{
			if (c == '\t')
				return ' ';
			const char *p = strchr(alpha, c);
			if (p)
				return (int)(p - alpha);
			return dfl;
		}

		static const int transtab[][sizeof(ALPHABET)];

		int state;

		inline void open_tag(void)
		{
			if (include_tags.size() > 0) {
				if (in_tag == NULL) {
					if (include_tags.have(ParmStr(storage))) {
						in_tag = new std::string(storage);
					}
					return;
				}
			}

			if (!ex_tag && exclude_tags.have(ParmStr(storage))) {
				ex_tag = new std::string(storage);
			}
		}
		inline void close_tag(void)
		{
			if (ex_tag && *ex_tag == storage) {
				delete ex_tag;
				ex_tag = NULL;
			}
			if (in_tag && *in_tag == storage) {
				delete in_tag;
				in_tag = NULL;
			}
		}
		inline bool next_char(FilterChar::Chr c)
		{
			int a = c2a(c);
			state = transtab[state][a];
			bool visible = false;

			if (state & C) {
				close_tag();
				storage.clear();
			} else if (state & O) {
				open_tag();
				storage.clear();
			} else if (state & R) {
				storage.clear();
			} else if (state & S) {
				storage.push_back(c);
			} else
				visible = (include_tags.size() == 0 || in_tag) &&
					  (ex_tag == NULL) &&
					  (state & V);

			state = STATE_OF(state);
			return visible;
		}

	public:
		PosibErr<bool> setup(Config *);
		void reset();
		void process(FilterChar * &, FilterChar * &);
	};

	PosibErr<bool> GCIDEFilter::setup(Config *cfg)
	{
		name_ = "gcide-filter";
		order_num_ = 0.2;
		include_tags.clear();
		cfg->retrieve_list("f-gcide-include-tags", &include_tags);
		exclude_tags.clear();
		if (cfg->retrieve_bool("f-gcide-skip-headwords")) {
			exclude_tags.add("ent");
			exclude_tags.add("hw");
		}
		cfg->retrieve_list("f-gcide-exclude-tags", &exclude_tags);
		reset();
		return true;
	}

	void GCIDEFilter::reset()
	{
		state = 0;
		storage.clear();
		ex_tag = NULL;
		in_tag = NULL;
	}

	void GCIDEFilter::process(FilterChar * & str, FilterChar * & stop)
	{
		FilterChar *cur = str;
		for (; cur != stop; cur++) {
			if (!next_char(*cur) && *cur != '\n')
				*cur = ' ';
		}
	}

	const char GCIDEFilter::alpha[] = ALPHABET;

	const int GCIDEFilter::transtab[][sizeof(alpha)] = {
		// ' ', '!', '"', '-', '/', '<', '>','\\', DFL
		{  0|V, 0|V, 0|V, 0|V, 0|V,   1, 0|V, 0|V, 0|V }, // 0
		{    2,   2, 9|S,   4,   8,   0,   0, 9|S, 9|S }, // 1
		{    3,   3,   2,   2,   2,   2,   2,   2,   2 }, // 2
		{    2,   2,   2,   2,   2,   2,   0,   2,   2 }, // 3
		{    4,   4,   4,   5,   4,   4,   4,   4,   4 }, // 4
		{    5,   5,   5,   6,   5,   5,   5,   5,   5 }, // 5
		{    4,   4,   4,   7,   4,   4,   4,   4,   4 }, // 6
		{    4,   4,   4,   4,   4,   4,   0,   4,   4 }, // 7
		{  8|S, 8|S, 8|S, 8|S, 8|S, 8|S, 0|C, 8|S, 8|S }, // 8
		{   10, 9|S, 9|S, 9|S, 0|R, 9|S, 0|O, 9|S, 9|S }, // 9
		{   10,  11,  10,  10,  10,  10, 0|O,  10,  10 }, // 10
		{   11,  10,  11,  11,  11,  11,  11,  12,  11 }, // 11
		{   11,  11,  11,  11,  11,  11,  11,  12,  11 }, // 12
		// ' ', '"', '!', '-', '/', '<', '>','\\'. DFL
	};
}

extern "C" IndividualFilter * new_aspell_gcide_filter() {
  return new GCIDEFilter;
}
